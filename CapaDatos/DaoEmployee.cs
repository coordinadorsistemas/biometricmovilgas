﻿using CapaEntidad;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CapaDatos
{
    public static class DaoEmployee
    {
        public static List<EntEmployee> GetEmployeeFromDB()
        {
            List<EntEmployee> objs = new List<EntEmployee>();
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            try
            {
                Conexion cn = new Conexion();
                SqlConnection cnx = cn.Conectar();
                string sql = "GetAllEmployeeAndFingerPrint";
                cmd = new SqlCommand(sql, cnx);
                cmd.CommandType = CommandType.StoredProcedure;
                cnx.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Console.WriteLine(dr["FingerIndex"].ToString());
                    Console.WriteLine(dr["TmpData"].ToString());
                    EntEmployee item = new EntEmployee
                    {
                        MachineNumber = 1,
                        EnrollNumber = dr["EnrollNumber"].ToString(),
                        Name = dr["Name"].ToString(),
                        Password = dr["Password"].ToString(),
                        Privilege = Int32.Parse(dr["Privilege"].ToString()),
                        Enabled = Boolean.Parse(dr["Enabled"].ToString()),
                        Cedula = dr["Cedula"].ToString(),
                        FingerIndex = dr["FingerIndex"].ToString().Length == 0 ? -1 : Int32.Parse(dr["FingerIndex"].ToString()),
                        TmpData = dr["TmpData"].ToString()
                    };
                    Console.WriteLine(item.Enabled);
                    objs.Add(item);
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
            finally
            {
                cmd.Connection.Close();
            }
            return objs;
        }

        
        public static List<EntEmployee> GetEmployeeFromDBWhitFingerprint()
        {
            List<EntEmployee> objs = new List<EntEmployee>();
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            try
            {
                Conexion cn = new Conexion();
                SqlConnection cnx = cn.Conectar();
                string sql = "GetAllEmployeeWhitFingerPrint";
                cmd = new SqlCommand(sql, cnx);
                cmd.CommandType = CommandType.StoredProcedure;
                cnx.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Console.WriteLine(dr["FingerIndex"].ToString());
                    Console.WriteLine(dr["TmpData"].ToString());
                    EntEmployee item = new EntEmployee
                    {
                        MachineNumber = 1,
                        EnrollNumber = dr["EnrollNumber"].ToString(),
                        Name = dr["Name"].ToString(),
                        Password = dr["Password"].ToString(),
                        Privilege = Int32.Parse(dr["Privilege"].ToString()),
                        Enabled = Boolean.Parse(dr["Enabled"].ToString()),
                        Cedula = dr["Cedula"].ToString(),
                        FingerIndex = dr["FingerIndex"].ToString().Length == 0 ? -1 : Int32.Parse(dr["FingerIndex"].ToString()),
                        TmpData = dr["TmpData"].ToString()
                    };
                    Console.WriteLine(item.Enabled);
                    objs.Add(item);
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
            finally
            {
                cmd.Connection.Close();
            }
            return objs;
        }

        public static List<EntEmployee> GetEmployeeFromDBWhitoutFingerprint()
        {
            List<EntEmployee> objs = new List<EntEmployee>();
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            try
            {
                Conexion cn = new Conexion();
                SqlConnection cnx = cn.Conectar();
                string sql = "GetAllEmployeeWhitoutFingerPrint";
                cmd = new SqlCommand(sql, cnx);
                cmd.CommandType = CommandType.StoredProcedure;
                cnx.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Console.WriteLine(dr["FingerIndex"].ToString());
                    Console.WriteLine(dr["TmpData"].ToString());
                    EntEmployee item = new EntEmployee
                    {
                        MachineNumber = 1,
                        EnrollNumber = dr["EnrollNumber"].ToString(),
                        Name = dr["Name"].ToString(),
                        Password = dr["Password"].ToString(),
                        Privilege = Int32.Parse(dr["Privilege"].ToString()),
                        Enabled = Boolean.Parse(dr["Enabled"].ToString()),
                        Cedula = dr["Cedula"].ToString(),
                        FingerIndex = dr["FingerIndex"].ToString().Length == 0 ? -1 : Int32.Parse(dr["FingerIndex"].ToString()),
                        TmpData = dr["TmpData"].ToString()
                    };
                    Console.WriteLine(item.Enabled);
                    objs.Add(item);
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
            finally
            {
                cmd.Connection.Close();
            }
            return objs;
        }




        public static EntEmployee InsertEmployeeToDB(EntEmployee user)
        {
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            try
            {
                Conexion cn = new Conexion();
                SqlConnection cnx = cn.Conectar();
                cmd = new SqlCommand("InsertEmployee", cnx);
                //Declara parametros
                cmd.Parameters.Add("@Privilege", SqlDbType.TinyInt);
                cmd.Parameters.Add("@Enabled", SqlDbType.Bit);
                cmd.Parameters.Add("@Estacion", SqlDbType.TinyInt);
                cmd.Parameters.Add("@HoraExtra", SqlDbType.Bit);
                //Inserta parametros.
                cmd.Parameters.AddWithValue("@Name", user.Name);
                cmd.Parameters.AddWithValue("@Password", user.Password);
                cmd.Parameters["@Privilege"].Value = user.Privilege;
                cmd.Parameters["@Enabled"].Value = user.Enabled == true ? 1 : 0;
                cmd.Parameters.AddWithValue("@Cedula", user.Cedula);
                cmd.Parameters["@Estacion"].Value = user.Estacion;
                cmd.Parameters["@HoraExtra"].Value = user.HoraExtra == true ? 1 : 0;
                cmd.CommandType = CommandType.StoredProcedure;
                cnx.Open();
                dr = cmd.ExecuteReader();
                dr.Read();
                user.EnrollNumber = dr["EnrollNumber"].ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                Console.WriteLine(ex.Message);
            }
            finally
            {
                cmd.Connection.Close();
            }
            return user;
        }


        public static EntEmployee SelectEmployee(string cedula)
        {
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            EntEmployee entEmployee = null;
            try
            {
                Conexion cn = new Conexion();
                SqlConnection cnx = cn.Conectar();
                cmd = new SqlCommand("SelectEmployee", cnx);
                cmd.Parameters.AddWithValue("@cedula", cedula);
                cmd.CommandType = CommandType.StoredProcedure;
                cnx.Open();
                dr = cmd.ExecuteReader();
                dr.Read();
                entEmployee = new EntEmployee
                {
                    Cedula = dr["Cedula"].ToString(),
                    Name = dr["Name"].ToString(),
                    EnrollNumber = dr["EnrollNumber"].ToString(),
                    Estacion = Int32.Parse(dr["Estacion"].ToString())
                };
                return entEmployee;
            }
            catch (Exception)
            {
                return null;
            }
            finally
            {
                cmd.Connection.Close();
            }
        }
    }
}
