﻿using CapaEntidad;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CapaDatos
{
    public static class DaoLog
    {
        public static bool InsertLogsFromBiometricToDB(EntLog entLog)
        {
            SqlCommand cmd = null;
            try
            {
                Conexion cn = new Conexion();
                SqlConnection cnx = cn.Conectar();
                cmd = new SqlCommand("InsertLogsFromBiometric", cnx);
                //Declara parametros
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@EnrollNumber", SqlDbType.Int);
                cmd.Parameters.Add("@VerifyMode", SqlDbType.TinyInt);
                cmd.Parameters.Add("@InOutMode", SqlDbType.TinyInt);
                cmd.Parameters.Add("@DateTime", SqlDbType.DateTime);
                //Asinga Parametros.
                cmd.Parameters["@EnrollNumber"].Value = entLog.IndRegID;
                cmd.Parameters["@VerifyMode"].Value = 0;
                cmd.Parameters["@InOutMode"].Value = entLog.InOutMode;
                cmd.Parameters["@DateTime"].Value = DateTime.Parse(entLog.DateTimeRecord);
                cnx.Open();
                cmd.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
            finally
            {
                cmd.Connection.Close();
            }
        }


        public static DataTable GetLogDataOrder(DateTime fechaIni, DateTime fechaFin)
        {
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            DataTable dt = new DataTable("Hoja1");
            try
            {
                Conexion cn = new Conexion();
                SqlConnection cnx = cn.Conectar();
                cmd = new SqlCommand("GetLogsWhitDataOrder", cnx);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@fechaIni", SqlDbType.Date);
                cmd.Parameters.Add("@fechaFin", SqlDbType.Date);
                cmd.Parameters["@fechaIni"].Value = fechaIni.Date;
                cmd.Parameters["@fechaFin"].Value = fechaFin.Date;
                cnx.Open();
                dr = cmd.ExecuteReader();
                dt.Columns.Add("Cedula", typeof(string));
                dt.Columns.Add("EnrollNumber", typeof(string));
                dt.Columns.Add("Entrada", typeof(string));
                dt.Columns.Add("Salida", typeof(string));
                dt.Columns.Add("CantidadHoras", typeof(string));
                dt.Columns.Add("Estacion", typeof(string));
                dt.Columns.Add("nombreEstacion", typeof(string));
                dt.Columns.Add("nombreCiudad", typeof(string));
                dt.Load(dr);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                cmd.Connection.Close();
            }
            return dt;
        }


        public static DataTable GetLogData(DateTime fechaIni, DateTime fechaFin)
        {
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            DataTable dt = new DataTable("Hoja2");
            try
            {
                Conexion cn = new Conexion();
                SqlConnection cnx = cn.Conectar();
                cmd = new SqlCommand("GetLogsWhitData", cnx);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@fechaIni", SqlDbType.Date);
                cmd.Parameters.Add("@fechaFin", SqlDbType.Date);
                cmd.Parameters["@fechaIni"].Value = fechaIni.Date;
                cmd.Parameters["@fechaFin"].Value = fechaFin.Date;
                cnx.Open();
                dr = cmd.ExecuteReader();
                dt.Columns.Add("Cedula", typeof(string));
                dt.Columns.Add("EnrollNumber", typeof(string));
                dt.Columns.Add("InOutMode", typeof(string));
                dt.Columns.Add("Estacion", typeof(string));
                dt.Columns.Add("DateTime", typeof(string));
                dt.Columns.Add("nombreEstacion", typeof(string));
                dt.Columns.Add("nombreCiudad", typeof(string));
                dt.Load(dr);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                cmd.Connection.Close();
            }
            return dt;
        }
    }
}
