﻿using CapaEntidad;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaDatos
{
    public class Conexion
    {

        public SqlConnection Conectar()
        {
            SqlConnection cn = new SqlConnection();
            string cadenaDeConexion = ParametrosDeAplicacion.ObtenerParametros().ConexionDB;
            cn.ConnectionString = cadenaDeConexion;
            return cn;
        }
    }

}
