﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaDatos
{
    public static class DaoEstacion
    {
        public static DataTable GetEstacionesFromDB()
        {
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            DataTable dt = new DataTable();
            try
            {
                Conexion cn = new Conexion();
                SqlConnection cnx = cn.Conectar();
                cmd = new SqlCommand("Select * from Estacion", cnx);
                cnx.Open();
                dr = cmd.ExecuteReader();
                dt.Columns.Add("idEstacion", typeof(string));
                dt.Columns.Add("nombreEstacion", typeof(string));
                dt.Load(dr);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }
            finally
            {
                cmd.Connection.Close();
            }
            return dt;
        }

    }
}
