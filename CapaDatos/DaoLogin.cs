﻿using CapaEntidad;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CapaDatos
{
    public static class DaoLogin
    {
        public static bool Login(string user, string pass)
        {
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            try
            {
                Conexion cn = new Conexion();
                SqlConnection cnx = cn.Conectar();
                cmd = new SqlCommand("LoginDB", cnx);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@user", user);
                cmd.Parameters.AddWithValue("@pass", pass);
                Console.WriteLine("dentro");
                cnx.Open();
                dr = cmd.ExecuteReader();
                dr.Read();
                EntUser entUSer = new EntUser
                {
                    idUsuario = dr["idUsuario"].ToString(),
                    Nombre = dr["primerNombre"].ToString() + dr["primerApellido"].ToString(),
                    Password = dr["Password"].ToString(),
                    Estado = Boolean.Parse(dr["Estado"].ToString())
                };
                Console.WriteLine(entUSer.Estado);
                if (entUSer != null && entUSer.Estado)
                    return true;
            }
            catch (Exception)
            {
                return false;
            }
            finally
            {
                cmd.Connection.Close();
            }
            return false;
        }
    }
}
