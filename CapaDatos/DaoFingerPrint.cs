﻿using CapaEntidad;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CapaDatos
{
    public static class DaoFingerPrint
    {
        public static bool InsertFingerPrint(EntFingerPrint fingerPrint)
        {
            SqlCommand cmd = null;
            try
            {
                Conexion cn = new Conexion();
                SqlConnection cnx = cn.Conectar();
                cmd = new SqlCommand("InsertFingerPrint", cnx);
                //Declara parametros
                cmd.Parameters.Add("@EnrollNumber", SqlDbType.Int);
                cmd.Parameters.Add("@FingerIndex", SqlDbType.TinyInt);
                //Inserta parametros.
                cmd.Parameters["@EnrollNumber"].Value = fingerPrint.EnrollNumber;
                cmd.Parameters["@FingerIndex"].Value = fingerPrint.FingerIndex;
                cmd.Parameters.AddWithValue("@TmpData", fingerPrint.TmpData);
                cmd.CommandType = CommandType.StoredProcedure;
                cnx.Open();
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
            finally
            {
                cmd.Connection.Close();
            }
            return true;
        }


        public static List<EntFingerPrint> GetAllFingerPrintFromDB()
        {
            List<EntFingerPrint> list = new List<EntFingerPrint>();
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            try
            {
                Conexion cn = new Conexion();
                SqlConnection cnx = cn.Conectar();
                cmd = new SqlCommand("Select * from Huella", cnx);
                cnx.Open();
                dr = cmd.ExecuteReader();
                //ACa Voy
                while (dr.Read())
                {
                    EntFingerPrint item = new EntFingerPrint
                    {
                        EnrollNumber = Int32.Parse(dr["EnrollNumber"].ToString()),
                        FingerIndex = Int32.Parse(dr["FingerIndex"].ToString()),
                        TmpData = dr["TmpData"].ToString(),
                    };
                    list.Add(item);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                cmd.Connection.Close();
            }
            return list;
        }

    }
}
