﻿namespace BiometricoMovilgas
{
    partial class Master
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btPing = new System.Windows.Forms.Button();
            this.btConectar = new System.Windows.Forms.Button();
            this.txtIdentyDevice = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtPortDevice = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtIpDevice = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.lbStatus = new System.Windows.Forms.Label();
            this.lbDeviceInfo = new System.Windows.Forms.Label();
            this.dgvRecords = new System.Windows.Forms.DataGridView();
            this.btUsers = new System.Windows.Forms.Button();
            this.btLogUsers = new System.Windows.Forms.Button();
            this.txtUser = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtPass = new System.Windows.Forms.TextBox();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvRecords)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btPing);
            this.groupBox1.Controls.Add(this.btConectar);
            this.groupBox1.Controls.Add(this.txtIdentyDevice);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.txtPortDevice);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.txtIpDevice);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(649, 46);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Datos de conexión";
            // 
            // btPing
            // 
            this.btPing.Location = new System.Drawing.Point(483, 16);
            this.btPing.Name = "btPing";
            this.btPing.Size = new System.Drawing.Size(75, 23);
            this.btPing.TabIndex = 7;
            this.btPing.Text = "Ping";
            this.btPing.UseVisualStyleBackColor = true;
            this.btPing.Click += new System.EventHandler(this.btPing_Click);
            // 
            // btConectar
            // 
            this.btConectar.Location = new System.Drawing.Point(384, 16);
            this.btConectar.Name = "btConectar";
            this.btConectar.Size = new System.Drawing.Size(75, 23);
            this.btConectar.TabIndex = 6;
            this.btConectar.Text = "Conectar";
            this.btConectar.UseVisualStyleBackColor = true;
            this.btConectar.Click += new System.EventHandler(this.btConectar_Click);
            // 
            // txtIdentyDevice
            // 
            this.txtIdentyDevice.Location = new System.Drawing.Point(303, 16);
            this.txtIdentyDevice.Name = "txtIdentyDevice";
            this.txtIdentyDevice.Size = new System.Drawing.Size(39, 20);
            this.txtIdentyDevice.TabIndex = 5;
            this.txtIdentyDevice.Text = "1";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(281, 19);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(16, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Id";
            // 
            // txtPortDevice
            // 
            this.txtPortDevice.Location = new System.Drawing.Point(202, 16);
            this.txtPortDevice.Name = "txtPortDevice";
            this.txtPortDevice.Size = new System.Drawing.Size(61, 20);
            this.txtPortDevice.TabIndex = 3;
            this.txtPortDevice.Text = "4370";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(158, 19);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(38, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Puerto";
            // 
            // txtIpDevice
            // 
            this.txtIpDevice.Location = new System.Drawing.Point(29, 16);
            this.txtIpDevice.Name = "txtIpDevice";
            this.txtIpDevice.Size = new System.Drawing.Size(113, 20);
            this.txtIpDevice.TabIndex = 1;
            this.txtIpDevice.Text = "192.168.10.80";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(17, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "IP";
            // 
            // lbStatus
            // 
            this.lbStatus.Location = new System.Drawing.Point(-1, 419);
            this.lbStatus.Name = "lbStatus";
            this.lbStatus.Size = new System.Drawing.Size(695, 22);
            this.lbStatus.TabIndex = 1;
            this.lbStatus.Text = "label4";
            this.lbStatus.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbDeviceInfo
            // 
            this.lbDeviceInfo.AutoSize = true;
            this.lbDeviceInfo.Location = new System.Drawing.Point(18, 61);
            this.lbDeviceInfo.Name = "lbDeviceInfo";
            this.lbDeviceInfo.Size = new System.Drawing.Size(135, 13);
            this.lbDeviceInfo.TabIndex = 2;
            this.lbDeviceInfo.Text = "Información de dispositivo: ";
            // 
            // dgvRecords
            // 
            this.dgvRecords.AllowUserToAddRows = false;
            this.dgvRecords.AllowUserToDeleteRows = false;
            this.dgvRecords.AllowUserToOrderColumns = true;
            this.dgvRecords.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvRecords.Location = new System.Drawing.Point(12, 127);
            this.dgvRecords.Name = "dgvRecords";
            this.dgvRecords.Size = new System.Drawing.Size(669, 287);
            this.dgvRecords.TabIndex = 3;
            // 
            // btUsers
            // 
            this.btUsers.Location = new System.Drawing.Point(21, 77);
            this.btUsers.Name = "btUsers";
            this.btUsers.Size = new System.Drawing.Size(75, 23);
            this.btUsers.TabIndex = 4;
            this.btUsers.Text = "Usuarios";
            this.btUsers.UseVisualStyleBackColor = true;
            this.btUsers.Click += new System.EventHandler(this.btUsers_Click);
            // 
            // btLogUsers
            // 
            this.btLogUsers.Location = new System.Drawing.Point(133, 77);
            this.btLogUsers.Name = "btLogUsers";
            this.btLogUsers.Size = new System.Drawing.Size(75, 23);
            this.btLogUsers.TabIndex = 5;
            this.btLogUsers.Text = "Registros";
            this.btLogUsers.UseVisualStyleBackColor = true;
            this.btLogUsers.Click += new System.EventHandler(this.btLogUsers_Click);
            // 
            // txtUser
            // 
            this.txtUser.Location = new System.Drawing.Point(315, 77);
            this.txtUser.Name = "txtUser";
            this.txtUser.Size = new System.Drawing.Size(113, 20);
            this.txtUser.TabIndex = 6;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(586, 75);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 34);
            this.button1.TabIndex = 8;
            this.button1.Text = "Agregar Usuario";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(279, 80);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(29, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "User";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(432, 81);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(30, 13);
            this.label5.TabIndex = 10;
            this.label5.Text = "Pass";
            // 
            // txtPass
            // 
            this.txtPass.Location = new System.Drawing.Point(467, 78);
            this.txtPass.Name = "txtPass";
            this.txtPass.Size = new System.Drawing.Size(113, 20);
            this.txtPass.TabIndex = 9;
            // 
            // Master
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(693, 441);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txtPass);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.txtUser);
            this.Controls.Add(this.btLogUsers);
            this.Controls.Add(this.btUsers);
            this.Controls.Add(this.dgvRecords);
            this.Controls.Add(this.lbDeviceInfo);
            this.Controls.Add(this.lbStatus);
            this.Controls.Add(this.groupBox1);
            this.Name = "Master";
            this.Text = "MovilgasBiometrics";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvRecords)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btPing;
        private System.Windows.Forms.Button btConectar;
        private System.Windows.Forms.TextBox txtIdentyDevice;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtPortDevice;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtIpDevice;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lbStatus;
        private System.Windows.Forms.Label lbDeviceInfo;
        private System.Windows.Forms.DataGridView dgvRecords;
        private System.Windows.Forms.Button btUsers;
        private System.Windows.Forms.Button btLogUsers;
        private System.Windows.Forms.TextBox txtUser;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtPass;
    }
}

