﻿namespace BiometricoMovilgas
{
    partial class FormBiometrico
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.chbxHoraExtra = new System.Windows.Forms.CheckBox();
            this.txbApellido = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txbPass = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txbCodigo = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.btnLimpiar = new System.Windows.Forms.Button();
            this.btnGuardar = new System.Windows.Forms.Button();
            this.cbxEstacion = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txbNombre = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txbCedula = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.btnDbToBiometric = new System.Windows.Forms.Button();
            this.BtnLogsBiometricToDB = new System.Windows.Forms.Button();
            this.btnConectar = new System.Windows.Forms.Button();
            this.lbDeviceInfo = new System.Windows.Forms.Label();
            this.lblPuerto = new System.Windows.Forms.Label();
            this.lblIp = new System.Windows.Forms.Label();
            this.btnBiometricToDb = new System.Windows.Forms.Button();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.label8 = new System.Windows.Forms.Label();
            this.dateTimePickerEnd = new System.Windows.Forms.DateTimePicker();
            this.label6 = new System.Windows.Forms.Label();
            this.dateTimePickerIni = new System.Windows.Forms.DateTimePicker();
            this.btnExportExcelLogs = new System.Windows.Forms.Button();
            this.lbStatus = new System.Windows.Forms.Label();
            this.btnDeleteUsers = new System.Windows.Forms.Button();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(13, 13);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(568, 294);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.chbxHoraExtra);
            this.tabPage1.Controls.Add(this.txbApellido);
            this.tabPage1.Controls.Add(this.label7);
            this.tabPage1.Controls.Add(this.groupBox1);
            this.tabPage1.Controls.Add(this.btnLimpiar);
            this.tabPage1.Controls.Add(this.btnGuardar);
            this.tabPage1.Controls.Add(this.cbxEstacion);
            this.tabPage1.Controls.Add(this.label3);
            this.tabPage1.Controls.Add(this.txbNombre);
            this.tabPage1.Controls.Add(this.label2);
            this.tabPage1.Controls.Add(this.txbCedula);
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(560, 268);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Crear Usuario";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // chbxHoraExtra
            // 
            this.chbxHoraExtra.AutoSize = true;
            this.chbxHoraExtra.Location = new System.Drawing.Point(242, 52);
            this.chbxHoraExtra.Name = "chbxHoraExtra";
            this.chbxHoraExtra.Size = new System.Drawing.Size(121, 17);
            this.chbxHoraExtra.TabIndex = 5;
            this.chbxHoraExtra.Text = "Calcula horas extras";
            this.chbxHoraExtra.UseVisualStyleBackColor = true;
            // 
            // txbApellido
            // 
            this.txbApellido.Location = new System.Drawing.Point(111, 85);
            this.txbApellido.Name = "txbApellido";
            this.txbApellido.Size = new System.Drawing.Size(100, 20);
            this.txbApellido.TabIndex = 3;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(28, 88);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(76, 13);
            this.label7.TabIndex = 9;
            this.label7.Text = "Primer Apellido";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txbPass);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.txbCodigo);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Location = new System.Drawing.Point(31, 121);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(485, 76);
            this.groupBox1.TabIndex = 8;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Datos para el Biometrico";
            // 
            // txbPass
            // 
            this.txbPass.Enabled = false;
            this.txbPass.Location = new System.Drawing.Point(318, 32);
            this.txbPass.Name = "txbPass";
            this.txbPass.Size = new System.Drawing.Size(100, 20);
            this.txbPass.TabIndex = 12;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(235, 35);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(61, 13);
            this.label5.TabIndex = 11;
            this.label5.Text = "Contraseña";
            // 
            // txbCodigo
            // 
            this.txbCodigo.Enabled = false;
            this.txbCodigo.Location = new System.Drawing.Point(102, 32);
            this.txbCodigo.Name = "txbCodigo";
            this.txbCodigo.Size = new System.Drawing.Size(100, 20);
            this.txbCodigo.TabIndex = 10;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(19, 35);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(40, 13);
            this.label4.TabIndex = 9;
            this.label4.Text = "Código";
            // 
            // btnLimpiar
            // 
            this.btnLimpiar.Location = new System.Drawing.Point(382, 78);
            this.btnLimpiar.Name = "btnLimpiar";
            this.btnLimpiar.Size = new System.Drawing.Size(134, 23);
            this.btnLimpiar.TabIndex = 7;
            this.btnLimpiar.Text = "Limpiar";
            this.btnLimpiar.UseVisualStyleBackColor = true;
            this.btnLimpiar.Click += new System.EventHandler(this.btnLimpiar_Click);
            // 
            // btnGuardar
            // 
            this.btnGuardar.Location = new System.Drawing.Point(242, 78);
            this.btnGuardar.Name = "btnGuardar";
            this.btnGuardar.Size = new System.Drawing.Size(134, 23);
            this.btnGuardar.TabIndex = 6;
            this.btnGuardar.Text = "Guardar";
            this.btnGuardar.UseVisualStyleBackColor = true;
            this.btnGuardar.Click += new System.EventHandler(this.btnGuardar_Click);
            // 
            // cbxEstacion
            // 
            this.cbxEstacion.FormattingEnabled = true;
            this.cbxEstacion.Location = new System.Drawing.Point(319, 16);
            this.cbxEstacion.Name = "cbxEstacion";
            this.cbxEstacion.Size = new System.Drawing.Size(197, 21);
            this.cbxEstacion.TabIndex = 4;
            this.cbxEstacion.SelectedIndexChanged += new System.EventHandler(this.cbxEstacion_SelectedIndexChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(239, 19);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(48, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Estación";
            // 
            // txbNombre
            // 
            this.txbNombre.Location = new System.Drawing.Point(111, 50);
            this.txbNombre.Name = "txbNombre";
            this.txbNombre.Size = new System.Drawing.Size(100, 20);
            this.txbNombre.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(28, 53);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(76, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Primer Nombre";
            // 
            // txbCedula
            // 
            this.txbCedula.Location = new System.Drawing.Point(111, 16);
            this.txbCedula.Name = "txbCedula";
            this.txbCedula.Size = new System.Drawing.Size(100, 20);
            this.txbCedula.TabIndex = 1;
            this.txbCedula.Leave += new System.EventHandler(this.txbCedula_Leave);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(28, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(40, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Cédula";
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.btnDeleteUsers);
            this.tabPage3.Controls.Add(this.groupBox2);
            this.tabPage3.Controls.Add(this.BtnLogsBiometricToDB);
            this.tabPage3.Controls.Add(this.btnConectar);
            this.tabPage3.Controls.Add(this.lbDeviceInfo);
            this.tabPage3.Controls.Add(this.lblPuerto);
            this.tabPage3.Controls.Add(this.lblIp);
            this.tabPage3.Controls.Add(this.btnBiometricToDb);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(560, 268);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Sincronizar";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.button1);
            this.groupBox2.Controls.Add(this.button2);
            this.groupBox2.Controls.Add(this.btnDbToBiometric);
            this.groupBox2.Location = new System.Drawing.Point(25, 118);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(200, 100);
            this.groupBox2.TabIndex = 11;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "DB To Biometric";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(16, 22);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(84, 32);
            this.button1.TabIndex = 9;
            this.button1.Text = "Sin Huellas";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(106, 22);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(84, 32);
            this.button2.TabIndex = 10;
            this.button2.Text = "Con Huellas";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // btnDbToBiometric
            // 
            this.btnDbToBiometric.Location = new System.Drawing.Point(59, 60);
            this.btnDbToBiometric.Name = "btnDbToBiometric";
            this.btnDbToBiometric.Size = new System.Drawing.Size(84, 32);
            this.btnDbToBiometric.TabIndex = 1;
            this.btnDbToBiometric.Text = "Todo";
            this.btnDbToBiometric.UseVisualStyleBackColor = true;
            this.btnDbToBiometric.Click += new System.EventHandler(this.btnDbToBiometric_Click);
            // 
            // BtnLogsBiometricToDB
            // 
            this.BtnLogsBiometricToDB.Location = new System.Drawing.Point(356, 118);
            this.BtnLogsBiometricToDB.Name = "BtnLogsBiometricToDB";
            this.BtnLogsBiometricToDB.Size = new System.Drawing.Size(84, 45);
            this.BtnLogsBiometricToDB.TabIndex = 8;
            this.BtnLogsBiometricToDB.Text = "Logs Biometric To DB";
            this.BtnLogsBiometricToDB.UseVisualStyleBackColor = true;
            this.BtnLogsBiometricToDB.Click += new System.EventHandler(this.LogsBiometricToDB_Click);
            // 
            // btnConectar
            // 
            this.btnConectar.Location = new System.Drawing.Point(396, 28);
            this.btnConectar.Name = "btnConectar";
            this.btnConectar.Size = new System.Drawing.Size(84, 45);
            this.btnConectar.TabIndex = 7;
            this.btnConectar.Text = "Conectar";
            this.btnConectar.UseVisualStyleBackColor = true;
            this.btnConectar.Click += new System.EventHandler(this.btnConectar_Click);
            // 
            // lbDeviceInfo
            // 
            this.lbDeviceInfo.AutoSize = true;
            this.lbDeviceInfo.Location = new System.Drawing.Point(40, 82);
            this.lbDeviceInfo.Name = "lbDeviceInfo";
            this.lbDeviceInfo.Size = new System.Drawing.Size(67, 13);
            this.lbDeviceInfo.TabIndex = 6;
            this.lbDeviceInfo.Text = "lbDeviceInfo";
            // 
            // lblPuerto
            // 
            this.lblPuerto.AutoSize = true;
            this.lblPuerto.Location = new System.Drawing.Point(190, 44);
            this.lblPuerto.Name = "lblPuerto";
            this.lblPuerto.Size = new System.Drawing.Size(35, 13);
            this.lblPuerto.TabIndex = 4;
            this.lblPuerto.Text = "label8";
            // 
            // lblIp
            // 
            this.lblIp.AutoSize = true;
            this.lblIp.Location = new System.Drawing.Point(40, 44);
            this.lblIp.Name = "lblIp";
            this.lblIp.Size = new System.Drawing.Size(35, 13);
            this.lblIp.TabIndex = 3;
            this.lblIp.Text = "label7";
            // 
            // btnBiometricToDb
            // 
            this.btnBiometricToDb.Location = new System.Drawing.Point(231, 118);
            this.btnBiometricToDb.Name = "btnBiometricToDb";
            this.btnBiometricToDb.Size = new System.Drawing.Size(84, 45);
            this.btnBiometricToDb.TabIndex = 2;
            this.btnBiometricToDb.Text = "Biometric To DB";
            this.btnBiometricToDb.UseVisualStyleBackColor = true;
            this.btnBiometricToDb.Click += new System.EventHandler(this.btnBiometricToDb_Click);
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.label8);
            this.tabPage2.Controls.Add(this.dateTimePickerEnd);
            this.tabPage2.Controls.Add(this.label6);
            this.tabPage2.Controls.Add(this.dateTimePickerIni);
            this.tabPage2.Controls.Add(this.btnExportExcelLogs);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Size = new System.Drawing.Size(560, 268);
            this.tabPage2.TabIndex = 3;
            this.tabPage2.Text = "Exportar";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(204, 34);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(62, 13);
            this.label8.TabIndex = 14;
            this.label8.Text = "Fecha Final";
            // 
            // dateTimePickerEnd
            // 
            this.dateTimePickerEnd.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePickerEnd.Location = new System.Drawing.Point(204, 53);
            this.dateTimePickerEnd.Name = "dateTimePickerEnd";
            this.dateTimePickerEnd.Size = new System.Drawing.Size(109, 20);
            this.dateTimePickerEnd.TabIndex = 13;
            this.dateTimePickerEnd.Value = new System.DateTime(2018, 1, 1, 0, 0, 0, 0);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(42, 34);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(67, 13);
            this.label6.TabIndex = 12;
            this.label6.Text = "Fecha Inicial";
            // 
            // dateTimePickerIni
            // 
            this.dateTimePickerIni.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePickerIni.Location = new System.Drawing.Point(42, 53);
            this.dateTimePickerIni.Name = "dateTimePickerIni";
            this.dateTimePickerIni.Size = new System.Drawing.Size(109, 20);
            this.dateTimePickerIni.TabIndex = 11;
            this.dateTimePickerIni.Value = new System.DateTime(2018, 1, 1, 0, 0, 0, 0);
            // 
            // btnExportExcelLogs
            // 
            this.btnExportExcelLogs.Location = new System.Drawing.Point(365, 34);
            this.btnExportExcelLogs.Name = "btnExportExcelLogs";
            this.btnExportExcelLogs.Size = new System.Drawing.Size(127, 39);
            this.btnExportExcelLogs.TabIndex = 10;
            this.btnExportExcelLogs.Text = "Expotar Logs en Excel";
            this.btnExportExcelLogs.UseVisualStyleBackColor = true;
            this.btnExportExcelLogs.Click += new System.EventHandler(this.btnExportExcelLogs_Click);
            // 
            // lbStatus
            // 
            this.lbStatus.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.lbStatus.Location = new System.Drawing.Point(0, 314);
            this.lbStatus.Name = "lbStatus";
            this.lbStatus.Size = new System.Drawing.Size(593, 13);
            this.lbStatus.TabIndex = 5;
            this.lbStatus.Text = "label9";
            this.lbStatus.Visible = false;
            // 
            // btnDeleteUsers
            // 
            this.btnDeleteUsers.Location = new System.Drawing.Point(296, 178);
            this.btnDeleteUsers.Name = "btnDeleteUsers";
            this.btnDeleteUsers.Size = new System.Drawing.Size(84, 45);
            this.btnDeleteUsers.TabIndex = 12;
            this.btnDeleteUsers.Text = "Borrar Usuarios";
            this.btnDeleteUsers.UseVisualStyleBackColor = true;
            this.btnDeleteUsers.Click += new System.EventHandler(this.btnDeleteUsers_Click);
            // 
            // FormBiometrico
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(593, 327);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.lbStatus);
            this.Name = "FormBiometrico";
            this.Text = "MovilGas - Biométrico";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FormBiometrico_FormClosed);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox txbPass;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txbCodigo;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btnLimpiar;
        private System.Windows.Forms.Button btnGuardar;
        private System.Windows.Forms.ComboBox cbxEstacion;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txbNombre;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txbCedula;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.Button btnDbToBiometric;
        private System.Windows.Forms.Button btnBiometricToDb;
        private System.Windows.Forms.Label lbStatus;
        private System.Windows.Forms.Label lblPuerto;
        private System.Windows.Forms.Label lblIp;
        private System.Windows.Forms.Label lbDeviceInfo;
        private System.Windows.Forms.Button btnConectar;
        private System.Windows.Forms.Button BtnLogsBiometricToDB;
        private System.Windows.Forms.TextBox txbApellido;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.DateTimePicker dateTimePickerIni;
        private System.Windows.Forms.Button btnExportExcelLogs;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.DateTimePicker dateTimePickerEnd;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.CheckBox chbxHoraExtra;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button btnDeleteUsers;
    }
}