﻿using CapaEntidad;
using CapaNegocio;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BiometricoMovilgas
{
    public partial class FormBiometrico : Form
    {
        DataTable dataEstacion = new DataTable();
        NegDeviceManipulator manipulator = new NegDeviceManipulator();
        public NegZkemClient objZkeeper;
        private bool isDeviceConnected = false;
        int device = 1;
        string ipAddress = ParametrosDeAplicacion.ObtenerParametros().IpDevice;
        string port = ParametrosDeAplicacion.ObtenerParametros().PortDevice;


        public FormBiometrico()
        {
            InitializeComponent();
            CargarEstaciones();
            this.StartPosition = FormStartPosition.CenterScreen;
            Controles(false);
            lblIp.Text = ipAddress;
            lblPuerto.Text = port;
            ControlInicial();
        }


        public void ControlInicial()
        {
            DateTime today = DateTime.Today;
            DateTime fechaIni = new DateTime(today.Year, today.Month, 1);
            DateTime fechaFin = new DateTime(today.Year, today.Month + 1, 1).AddDays(-1);
            dateTimePickerIni.Value = fechaIni;
            dateTimePickerEnd.Value = fechaFin;
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            string nombre = txbApellido.Text.Trim() + txbNombre.Text.Trim();
            string ced = txbCedula.Text.Trim();
            int cedNum;
            cedNum = NegEmployee.ValidarCamposSavEmployee(ced, nombre);

            if (cedNum == 0)
                return;

            string pass = ced.Substring(ced.Length - 4);
            EntEmployee obj = new EntEmployee()
            {
                Name = nombre,
                Password = pass,
                Privilege = 0,
                Enabled = true,
                Cedula = cedNum.ToString(),
                Estacion = Int32.Parse(dataEstacion.Rows[cbxEstacion.SelectedIndex].ItemArray[0].ToString()),
                HoraExtra = chbxHoraExtra.Checked
            };

            obj = NegEmployee.InsertUsuarioToDB(obj);

            Limpiar();
            if (obj.EnrollNumber.Length > 0)
            {
                txbCodigo.Text = obj.EnrollNumber;
                txbPass.Text = pass;
                MessageBox.Show("Guardado Exitoso");
                ShowStatusBar("Guardado Exitoso", true);
            }
            else
                ShowStatusBar("Error inexperado al guardar.", false);
        }

        public void CargarEstaciones()
        {
            dataEstacion = NegEstacion.GetEstacionesFromDB();
            cbxEstacion.ValueMember = "idEstacion";
            cbxEstacion.DisplayMember = "nombreEstacion";
            cbxEstacion.DataSource = dataEstacion;
        }

        private void cbxEstacion_SelectedIndexChanged(object sender, EventArgs e)
        {
        }

        private void btnLimpiar_Click(object sender, EventArgs e)
        {
            Limpiar();
        }

        public void Limpiar()
        {
            txbCedula.Text = String.Empty;
            txbCodigo.Text = String.Empty;
            txbNombre.Text = String.Empty;
            txbPass.Text = String.Empty;
            txbApellido.Text = string.Empty;
        }

        private void btnConectar_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;
                ShowStatusBar(string.Empty, true);

                if (IsDeviceConnected)
                {
                    IsDeviceConnected = false;
                    this.Cursor = Cursors.Default;

                    return;
                }

                if (ipAddress == string.Empty || port == string.Empty)
                    throw new Exception("The Device IP Address and Port is mandotory !!");

                int portNumber = 4370;
                if (!int.TryParse(port, out portNumber))
                    throw new Exception("Not a valid port number");

                bool isValidIpA = NegUniversalStatic.ValidateIP(ipAddress);
                if (!isValidIpA)
                    throw new Exception("The Device IP is invalid !!");

                isValidIpA = NegUniversalStatic.PingTheDevice(ipAddress);
                if (!isValidIpA)
                    throw new Exception("The device at " + ipAddress + ":" + port + " did not respond!!");

                objZkeeper = new NegZkemClient(RaiseDeviceEvent);
                IsDeviceConnected = objZkeeper.Connect_Net(ipAddress, portNumber);

                if (IsDeviceConnected)
                {
                    string deviceInfo = manipulator.FetchDeviceInfo(objZkeeper, device);
                    lbDeviceInfo.Text = deviceInfo;
                }
                Controles(true);

            }
            catch (Exception ex)
            {
                ShowStatusBar(ex.Message, false);
            }
            this.Cursor = Cursors.Default;

        }

        public bool IsDeviceConnected
        {
            get { return isDeviceConnected; }
            set
            {
                isDeviceConnected = value;
                if (isDeviceConnected)
                {
                    ShowStatusBar("The device is connected !!", true);
                    btnConectar.Text = "Desconectar";
                    Controles(true);
                }
                else
                {
                    ShowStatusBar("The device is diconnected !!", true);
                    objZkeeper.Disconnect();
                    btnConectar.Text = "Connectar";
                    Controles(false);
                }
            }
        }

        private void RaiseDeviceEvent(object sender, string actionType)
        {
            switch (actionType)
            {
                case NegUniversalStatic.acx_Disconnect:
                    {
                        ShowStatusBar("The device is switched off", true);
                        btnConectar.Text = "Connectar";
                        Controles(false);
                        break;
                    }

                default:
                    break;
            }

        }

        public void ShowStatusBar(string message, bool type)
        {
            if (message.Trim() == string.Empty)
            {
                lbStatus.Visible = false;
                return;
            }

            lbStatus.Visible = true;
            lbStatus.Text = message;
            lbStatus.ForeColor = Color.White;

            if (type)
                lbStatus.BackColor = Color.FromArgb(79, 208, 154);
            else
                lbStatus.BackColor = Color.FromArgb(230, 112, 134);
        }

        private void btnBiometricToDb_Click(object sender, EventArgs e)
        {
            InsertFingerPrintToDB();
            //ClearData(5);
        }

        private void btnDbToBiometric_Click(object sender, EventArgs e)
        {
            this.Cursor = Cursors.WaitCursor;
            try
            {
                List<EntEmployee> users = new List<EntEmployee>();
                users = NegEmployee.GetEmployeeFromDB();
                int cont = manipulator.PushUserDataToDevice(users, objZkeeper, device);
                ShowStatusBar(cont + " Usuarios actualizados en el Biometrico.", true);
            }
            catch (Exception ex)
            {
                ShowStatusBar(ex.Message, false);
            }
            this.Cursor = Cursors.Default;
        }

        public void Controles(bool control)
        {
            button1.Enabled = false;
            button2.Enabled = false;
            btnBiometricToDb.Enabled = false;
            btnDbToBiometric.Enabled = false;
            BtnLogsBiometricToDB.Enabled = control;
            btnDeleteUsers.Enabled = false;
        }


        public void InsertFingerPrintToDB()
        {
            this.Cursor = Cursors.WaitCursor;
            try
            {
                ICollection<EntEmployee> list = manipulator.GetAllUserInfo(objZkeeper, device);
                if (list != null && list.Count > 0)
                {
                    ShowStatusBar(list.Count + " records found !!", true);
                    int cont = NegFingerPrint.InsertFingerPrint(list);
                    ShowStatusBar(cont + " han sido guardados correctamente!!", true);
                }
                else
                    ShowStatusBar("No records found", false);
            }
            catch (Exception ex)
            {
                ShowStatusBar(ex.Message, false);
            }
            this.Cursor = Cursors.Default;
        }

        //no elimina el que es
        public void DeleteUserOnBiometric()
        {
            this.Cursor = Cursors.WaitCursor;
            try
            {
                Console.WriteLine(objZkeeper.DeleteEnrollData(device, device, 920001, 12));
                Console.WriteLine(objZkeeper.DeleteEnrollData(device, device, 920001, 10));
                ShowStatusBar(1 + "han sido Eliminados correctamente!!", true);
            }
            catch (Exception ex)
            {
                ShowStatusBar(ex.Message, false);
            }
            this.Cursor = Cursors.Default;
        }

        /// <summary>
        /// Elimina segun el parametro
        /// </summary>
        /// <param name="value">1 Attendance records, 2 Fingerprint template data, 3 None, 4 Operation records, 5 User information</param>
        public void ClearData(int value)
        {
            this.Cursor = Cursors.WaitCursor;
            try
            {
                objZkeeper.ClearData(device, value);
                ShowStatusBar("Proceso realizado correctamente!!", true);
            }
            catch (Exception ex)
            {
                ShowStatusBar(ex.Message, false);
            }
            this.Cursor = Cursors.Default;

        }

        private void LogsBiometricToDB_Click(object sender, EventArgs e)
        {
            this.Cursor = Cursors.WaitCursor;
            try
            {
                ICollection<EntLog> listLogs = manipulator.GetLogData(objZkeeper, device);
                int cont = NegLog.InsertLogsFromBiometricToDB(listLogs);
                if (listLogs.Count == cont && cont != 0)
                    ClearData(1);
                ShowStatusBar(cont + " Registros guardados.", true);
            }
            catch (Exception ex)
            {
                ShowStatusBar(ex.Message, false);
            }
            this.Cursor = Cursors.Default;
        }

        private void btnExportExcelLogs_Click(object sender, EventArgs e)
        {
            DateTime fechaIni = DateTime.Parse(dateTimePickerIni.Text);
            DateTime fechaFin = DateTime.Parse(dateTimePickerEnd.Text);
            FolderBrowserDialog folderBrowserDialog = new FolderBrowserDialog();
            folderBrowserDialog.RootFolder = Environment.SpecialFolder.Desktop;
            if (folderBrowserDialog.ShowDialog() == DialogResult.OK)
            {
                string path = folderBrowserDialog.SelectedPath;
                if (NegLog.GetLogData(path, fechaIni, fechaFin))
                    ShowStatusBar("Archivo exportado con éxito.", true);
                else
                    ShowStatusBar("Error inexperado al exportar el archivo.", false);
            }
        }

        private void FormBiometrico_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        private void txbCedula_Leave(object sender, EventArgs e)
        {
            EntEmployee ent = NegEmployee.SelectEmployee(txbCedula.Text);
            if (ent != null)
            {
                MessageBox.Show("Empleado ya registrado.\n EnrollNumber : " + ent.EnrollNumber + "\n Identificación: " + ent.Cedula);
                Limpiar();
                txbCedula.Focus();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Cursor = Cursors.WaitCursor;
            try
            {
                List<EntEmployee> users = new List<EntEmployee>();
                users = NegEmployee.GetEmployeeFromDBWhitoutFingerprint();
                int cont = manipulator.PushUserDataToDevice(users, objZkeeper, device);
                ShowStatusBar(cont + " Usuarios actualizados en el Biometrico.", true);
            }
            catch (Exception ex)
            {
                ShowStatusBar(ex.Message, false);
            }
            this.Cursor = Cursors.Default;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Cursor = Cursors.WaitCursor;
            try
            {
                List<EntEmployee> users = new List<EntEmployee>();
                users = NegEmployee.GetEmployeeFromDBWhitFingerprint();
                int cont = manipulator.PushUserDataToDevice(users, objZkeeper, device);
                ShowStatusBar(cont + " Usuarios actualizados en el Biometrico.", true);
            }
            catch (Exception ex)
            {
                ShowStatusBar(ex.Message, false);
            }
            this.Cursor = Cursors.Default;
        }

        private void btnDeleteUsers_Click(object sender, EventArgs e)
        {
            this.Cursor = Cursors.WaitCursor;
            try
            {
                ClearData(5);
                ShowStatusBar("Registros Borrados.", true);
            }
            catch (Exception ex)
            {
                ShowStatusBar(ex.Message, false);
            }
            this.Cursor = Cursors.Default;
        }
    }
}
