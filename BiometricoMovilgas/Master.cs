﻿
using CapaEntidad;
using CapaNegocio;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BiometricoMovilgas
{
    public partial class Master : Form
    {
        NegDeviceManipulator manipulator = new NegDeviceManipulator();
        public NegZkemClient objZkeeper;
        private bool isDeviceConnected = false;

        public bool IsDeviceConnected
        {
            get { return isDeviceConnected; }
            set
            {
                isDeviceConnected = value;
                if (isDeviceConnected)
                {
                    ShowStatusBar("The device is connected !!", true);
                    btConectar.Text = "Desconectar";
                    ToggleControls(true);
                }
                else
                {
                    ShowStatusBar("The device is diconnected !!", true);
                    objZkeeper.Disconnect();
                    btConectar.Text = "Connectar";
                    ToggleControls(false);
                }
            }
        }

        private void ToggleControls(bool value)
        {
            btUsers.Enabled = value;
            btLogUsers.Enabled = value;
            txtIdentyDevice.Enabled = !value;
            txtPortDevice.Enabled = !value;
            txtIpDevice.Enabled = !value;
        }

        public Master()
        {
            InitializeComponent();
            ToggleControls(false);
            ShowStatusBar(string.Empty, true);
            DisplayEmpty();
        }


        /// <summary>
        /// Your Events will reach here if implemented
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="actionType"></param>
        private void RaiseDeviceEvent(object sender, string actionType)
        {
            switch (actionType)
            {
                case NegUniversalStatic.acx_Disconnect:
                    {
                        ShowStatusBar("The device is switched off", true);
                        DisplayEmpty();
                        btConectar.Text = "Connectar";
                        ToggleControls(false);
                        break;
                    }

                default:
                    break;
            }

        }

        public void ShowStatusBar(string message, bool type)
        {
            if (message.Trim() == string.Empty)
            {
                lbStatus.Visible = false;
                return;
            }

            lbStatus.Visible = true;
            lbStatus.Text = message;
            lbStatus.ForeColor = Color.White;

            if (type)
                lbStatus.BackColor = Color.FromArgb(79, 208, 154);
            else
                lbStatus.BackColor = Color.FromArgb(230, 112, 134);
        }


        private void ClearGrid()
        {
            if (dgvRecords.Controls.Count > 2)
            { dgvRecords.Controls.RemoveAt(2); }


            dgvRecords.DataSource = null;
            dgvRecords.Controls.Clear();
            dgvRecords.Rows.Clear();
            dgvRecords.Columns.Clear();
        }

        private void BindToGridView(object list)
        {
            ClearGrid();
            dgvRecords.DataSource = list;
            dgvRecords.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            NegUniversalStatic.ChangeGridProperties(dgvRecords);
        }

        private void DisplayListOutput(string message)
        {
            if (dgvRecords.Controls.Count > 2)
            { dgvRecords.Controls.RemoveAt(2); }

            ShowStatusBar(message, false);
        }

        private void DisplayEmpty()
        {
            ClearGrid();
            //dgvRecords.Controls.Add(new DataEmpty());
        }

        private void btConectar_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;
                ShowStatusBar(string.Empty, true);

                if (IsDeviceConnected)
                {
                    IsDeviceConnected = false;
                    this.Cursor = Cursors.Default;

                    return;
                }

                string ipAddress = txtIpDevice.Text.Trim();
                string port = txtPortDevice.Text.Trim();
                if (ipAddress == string.Empty || port == string.Empty)
                    throw new Exception("The Device IP Address and Port is mandotory !!");

                int portNumber = 4370;
                if (!int.TryParse(port, out portNumber))
                    throw new Exception("Not a valid port number");

                bool isValidIpA = NegUniversalStatic.ValidateIP(ipAddress);
                if (!isValidIpA)
                    throw new Exception("The Device IP is invalid !!");

                isValidIpA = NegUniversalStatic.PingTheDevice(ipAddress);
                if (!isValidIpA)
                    throw new Exception("The device at " + ipAddress + ":" + port + " did not respond!!");

                objZkeeper = new NegZkemClient(RaiseDeviceEvent);
                IsDeviceConnected = objZkeeper.Connect_Net(ipAddress, portNumber);

                if (IsDeviceConnected)
                {
                    string deviceInfo = manipulator.FetchDeviceInfo(objZkeeper, int.Parse(txtIdentyDevice.Text.Trim()));
                    lbDeviceInfo.Text = deviceInfo;
                }

            }
            catch (Exception ex)
            {
                ShowStatusBar(ex.Message, false);
            }
            this.Cursor = Cursors.Default;

        }

        private void btPing_Click(object sender, EventArgs e)
        {
            ShowStatusBar(string.Empty, true);

            string ipAddress = txtIpDevice.Text.Trim();

            bool isValidIpA = NegUniversalStatic.ValidateIP(ipAddress);
            if (!isValidIpA)
                throw new Exception("The Device IP is invalid !!");

            isValidIpA = NegUniversalStatic.PingTheDevice(ipAddress);
            if (isValidIpA)
                ShowStatusBar("The device is active", true);
            else
                ShowStatusBar("Could not read any response", false);
        }

        private void btUsers_Click(object sender, EventArgs e)
        {
            try
            {
                ShowStatusBar(string.Empty, true);

                ICollection<EntEmployee> lstFingerPrintTemplates = manipulator.GetAllUserInfo(objZkeeper, int.Parse(txtIdentyDevice.Text.Trim()));
                if (lstFingerPrintTemplates != null && lstFingerPrintTemplates.Count > 0)
                {
                    BindToGridView(lstFingerPrintTemplates);
                    ShowStatusBar(lstFingerPrintTemplates.Count + " records found !!", true);
                }
                else
                    DisplayListOutput("No records found");
            }
            catch (Exception ex)
            {
                DisplayListOutput(ex.Message);
            }

        }

        private void btLogUsers_Click(object sender, EventArgs e)
        {
            try
            {
                ShowStatusBar(string.Empty, true);

                ICollection<EntLog> lstMachineInfo = manipulator.GetLogData(objZkeeper, int.Parse(txtIdentyDevice.Text.Trim()));

                if (lstMachineInfo != null && lstMachineInfo.Count > 0)
                {
                    BindToGridView(lstMachineInfo);
                    ShowStatusBar(lstMachineInfo.Count + " records found !!", true);
                }
                else
                    DisplayListOutput("No records found");
            }
            catch (Exception ex)
            {
                DisplayListOutput(ex.Message);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            FormBiometrico biometrico = new FormBiometrico
            {
                Visible = true
            };
            this.Dispose(false);

            ////Agregar Usuario
            //try
            //{
            //    ShowStatusBar(string.Empty, true);
            //    if (manipulator.PushUserDataToDevice(objZkeeper, Int32.Parse(txtIdentyDevice.Text), "911", txtUser.Text, txtPass.Text, true, 1))
            //    {
            //        ShowStatusBar(" Saved !!", true);
            //    }
            //    else
            //        DisplayListOutput("Error.");
            //}
            //catch (Exception ex)
            //{
            //    DisplayListOutput(ex.Message);
            //    throw;
            //}

            //// Inactiva Discpositivo.
            //objZkeeper.EnableDevice(Int32.Parse(txtIdentyDevice.Text), false);
        }
    }
}
