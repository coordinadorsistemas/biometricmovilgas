﻿using CapaNegocio;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BiometricoMovilgas
{
    public partial class Login : Form
    {
        public Login()
        {
            InitializeComponent();
            this.StartPosition = FormStartPosition.CenterScreen;
        }

        private void btnIngresar_Click(object sender, EventArgs e)
        {
            try
            {
                string user = txbUser.Text.Trim();
                string pass = txbPass.Text.Trim();

                if(user == string.Empty || pass == string.Empty)
                    throw new Exception("Usuario y contraseña no deben estar vacíos.");

                if (NegLogin.Login(user, pass))
                {
                    FormBiometrico fb = new FormBiometrico();
                    fb.Show();
                    this.Hide();
                }
                else
                    MessageBox.Show("Error: Revisar Nombre, usuario y estado.");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
