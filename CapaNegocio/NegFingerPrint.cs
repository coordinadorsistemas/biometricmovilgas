﻿using CapaDatos;
using CapaEntidad;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaNegocio
{
    public static class NegFingerPrint
    {
        public static int InsertFingerPrint(ICollection<EntEmployee> entFingerPrint)
        {
            int cont = 0;
            ICollection<EntEmployee> list = entFingerPrint;
            foreach (EntEmployee item in list)
            {
                EntFingerPrint entFinger = new EntFingerPrint
                {
                    EnrollNumber = Int32.Parse(item.EnrollNumber),
                    FingerIndex = item.FingerIndex,
                    TmpData = item.TmpData
                };
                if (DaoFingerPrint.InsertFingerPrint(entFinger))
                    cont++;
            }
            return cont;
        }
    }
}
