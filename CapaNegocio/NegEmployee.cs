﻿using CapaEntidad;
using CapaDatos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CapaNegocio
{
    public static class NegEmployee
    {
        public static List<EntEmployee> GetEmployeeFromDB()
        {
            return DaoEmployee.GetEmployeeFromDB();
        }

        public static List<EntEmployee> GetEmployeeFromDBWhitFingerprint()
        {
            return DaoEmployee.GetEmployeeFromDBWhitFingerprint();
        }

        public static List<EntEmployee> GetEmployeeFromDBWhitoutFingerprint()
        {
            return DaoEmployee.GetEmployeeFromDBWhitoutFingerprint();
        }




        public static EntEmployee InsertUsuarioToDB(EntEmployee user)
        {
            return DaoEmployee.InsertEmployeeToDB(user);
        }

        public static int ValidarCamposSavEmployee(string cedula, string nombre)
        {
            int cedNum = 0;
            try
            {
                // Validaciones
                if (cedula == string.Empty || nombre == string.Empty)
                    throw new Exception("Debe llenar cédula y nombre.");

                if (!int.TryParse(cedula, out cedNum))
                    throw new Exception("Cedula Invalida");

                if (cedula.Length < 4 )
                    throw new Exception("El campo de cedula debe tener cuatro o más digitos.");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return 0;
            }
                return cedNum;
            // fin Validaciones
        }

        public static EntEmployee SelectEmployee(string cedula)
        {
            try
            {
                string ced = cedula.Trim();
                int numcel = 0;
                if (!int.TryParse(cedula, out numcel))
                    throw new Exception("Cedula Invalida");
                return DaoEmployee.SelectEmployee(cedula);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return null;
            }
        }
    }
}
