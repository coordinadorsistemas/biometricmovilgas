﻿using CapaDatos;
using CapaEntidad;
using ClosedXML.Excel;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CapaNegocio
{
    public static class NegLog
    {
        public static int InsertLogsFromBiometricToDB(ICollection<EntLog> entLog)
        {
            ICollection<EntLog> ent = entLog;
            int cont = 0;
            foreach (EntLog item in ent)
            {
                if (DaoLog.InsertLogsFromBiometricToDB(item))
                    cont++;
            }
            return cont;
        }

        public static bool GetLogData(string Path, DateTime fechaIni, DateTime fechaFin)
        {
            try
            {
                var wb = new XLWorkbook();
                DataTable data = DaoLog.GetLogDataOrder(fechaIni, fechaFin);
                DataTable data2 = DaoLog.GetLogData(fechaIni, fechaFin);
                wb.Worksheets.Add(data);
                wb.Worksheets.Add(data2);
                wb.SaveAs(Path + "\\RecordLog.xlsx");
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
        }
    }
}
