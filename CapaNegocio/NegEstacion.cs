﻿using CapaDatos;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaNegocio
{
    public static class NegEstacion
    {
        public static DataTable GetEstacionesFromDB()
        {
            return DaoEstacion.GetEstacionesFromDB();
        }
    }
}
