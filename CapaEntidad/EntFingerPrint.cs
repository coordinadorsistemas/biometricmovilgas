﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaEntidad
{
    public class EntFingerPrint
    {
        public int EnrollNumber { get; set; }
        public int FingerIndex { get; set; }
        public string TmpData { get; set; }
    }
}
