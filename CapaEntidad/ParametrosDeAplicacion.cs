﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaEntidad
{
    public class ParametrosDeAplicacion
    {
        public string ConexionDB;
        public string IpDevice;
        public string PortDevice;

        public static ParametrosDeAplicacion ObtenerParametros()
        {
            StreamReader objReader = new StreamReader("C:\\Biometrico\\parametros.json");
            string jsonParametros = objReader.ReadToEnd();

            ParametrosDeAplicacion pda = JsonConvert.DeserializeObject<ParametrosDeAplicacion>(jsonParametros);
            return pda;
        }
    }
}
