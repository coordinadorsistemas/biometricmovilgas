﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaEntidad
{
    public class EntEstacion
    {
        public int idEstacion { get; set; }
        public string nombreEstacion { get; set; }
        public int ciudadEstacion { get; set; }
    }
}
